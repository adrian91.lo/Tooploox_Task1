/* 
 * Created on: 17.12.2014
 * Author: Adrian Galeziowski
 *
 */
#ifndef IMAGE_H_
#define IMAGE_H_

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>


using namespace std;
using namespace cv;


/* Class defines interface to read in and handle images
 */
class Image{
private:
    Mat Im;				// image
    Size S;				// image size

public:
    Image(){

    }
	// IMAGE OPERATIONS
    int ReadImage(string File);                     //Function read in images
    int SaveImage(std::string File);				//Function save images to file
    int SaveImage(std::string File, int nr);
    void InitImage(int *size, unsigned short *buff);
	string type2str(int type);						// Returns human readable OpenCV matrix type
    void rotate(cv::Mat& src, double angle, cv::Mat& dst);

    void ChangeImage(Mat *Im_loc){ (*Im_loc).copyTo(Im);}
    int SaveImagePGM(unsigned short *buff, int *size);
    void ConvertToGray(void);

	// RETURNS
    inline Mat* ReturnImage(void) { return &Im; }    //Function returns Im
    inline int ReturnImageWidth(void) const { return S.width; }
    inline int ReturnImageHeight(void) const { return S.height; }
    void ShowImage(void);

};

#endif

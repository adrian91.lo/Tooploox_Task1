#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

namespace alg {
class Algorithm
{
private:
    Mat gradient_window;
    Mat sum_window;

    int CreateGradientWindow(const int size_);
    int CreateSumWindow(const int size_);
    int CalculateSigmoid(Mat &fcn, Mat &res);
public:
    Algorithm();
    Algorithm(int size);
    int Process(Mat *Im1, Mat *Im2, Mat *res, Mat *map, int nb, int depth);
    void Invert_Map(Mat *map);

};
}
#endif // ALGORITHM_H

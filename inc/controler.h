#ifndef CONTROLER_H
#define CONTROLER_H

#include <vector>
#include <string>
#include "Image.h"
#include "algorithm.h"

namespace ctrl {
class Controler
{
private:
    vector<Image> vecColorIm;
    vector<Image> vecGrayIm;
    alg::Algorithm Alg1;
public:
    int ReadVectorOfImages(string path_, int nb_);
    void ShowVectorOfImages();
    int JoinImages();
};

}//end of namespace ctrl

#endif // CONTROLER_H

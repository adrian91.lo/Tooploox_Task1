/* 
 * Created on: 17.12.2014
 * Author: Adrian Galeziowski
 *
*/
#include "Image.h"

int Image::ReadImage(string File){
    Im = imread( File, CV_LOAD_IMAGE_COLOR);
    if(! Im.data ){                              // Check for invalid input
        cerr <<  "Could not open or find image"<<endl ;
        return -1;
	}
    S = Im.size();
	return 0;
}

int Image::SaveImage(std::string File){
    std::string roz = ".png";
    imwrite(File+roz, Im );
	return 0;
}

int Image::SaveImage(std::string File, int nr){
    ostringstream tmp;
    tmp<<nr;
    std::string roz = "_"+tmp.str()+".png";


    vector<int> compression_params;
    compression_params.push_back(CV_IMWRITE_PXM_BINARY);
    compression_params.push_back(0);

    imwrite(File+roz, Im, compression_params);
    return 0;
}

/**
 * Rotate an image
 */
void rotate(cv::Mat& src, double angle, cv::Mat& dst)
{
    Point2f src_center(src.cols/2.0F, src.rows/2.0F);
    Mat rot_mat = getRotationMatrix2D(src_center, angle, 1.0);
    warpAffine(src, dst, rot_mat, src.size());
}

string Image::type2str(int type) {
  string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}


void Image::ShowImage(void){
    namedWindow("Image",WINDOW_AUTOSIZE);
    imshow("Image",Im);
    waitKey();
}

void Image::InitImage(int *size, unsigned short *buff){
       Im = Mat(size[1], size[0], CV_8UC1, buff);
}

int Image::SaveImagePGM(unsigned short *buff, int *size){
    static int nr = 0;
    ostringstream tmp;
    tmp<<nr;
    std::string name = "Images/Im_"+tmp.str()+".pgm";
    ofstream StrmWy(name.c_str());
    StrmWy<<"P2\n"<<size[0]<<" "<<size[1]<<" 1024"<<endl;
    int cnt = 0;
    for(int i = 0; i < size[1]; i++){
        for(int j = 0; j < size[0]; j++){
            StrmWy<<buff[cnt]<<" ";
            cnt++;
        }
        StrmWy<<endl;
    }
    StrmWy.close();
    nr++;
    return (nr-1);
}

void Image::ConvertToGray(){
    cvtColor(Im,Im,COLOR_RGB2GRAY);
}

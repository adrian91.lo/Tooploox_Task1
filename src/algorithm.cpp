#include <iostream>
#include "algorithm.h"

namespace alg {

Algorithm::Algorithm(){
    CreateGradientWindow(5);
    CreateSumWindow(5);
}

Algorithm::Algorithm(int size){
    CreateGradientWindow(size);
    CreateSumWindow(size);
}


int Algorithm::CreateGradientWindow(const int size_){
    if(size_ <= 0){
        cerr<<"[ERROR][CreateGradientWindow] Wrong window size"<<endl;
        return -1;
    }
    int center = size_/2;
    gradient_window.create(size_,size_,CV_32FC1);
    for(int i = 0; i < size_; i++){
        for(int j = 0; j < size_; j++){
            gradient_window.at<float>(Point(i,j)) = 0;
            if((i == center - 1 && j == center) || (i == center && j == center - 1))
                gradient_window.at<float>(Point(i,j)) = -1;
        }
    }
    gradient_window.at<float>(Point(center,center)) = 2;

    return 0;
}

int Algorithm::CreateSumWindow(const int size_){
    if(size_ <= 0){
        cerr<<"[ERROR][CreateSumWindow] Wrong window size"<<endl;
        return -1;
    }
    sum_window.create(size_,size_,CV_32FC1);
    for(int i = 0; i < size_; i++){
        for(int j = 0; j < size_; j++){
            sum_window.at<float>(Point(i,j)) = 1;
        }
    }
    return 0;
}


int Algorithm::CalculateSigmoid(Mat &fcn, Mat &res){
    int status = 0;
    Mat tmp, tmp2;

    Mat dst = (fcn >= 0) & 1;
    dst.convertTo(dst,CV_32F, 2.0, -1.0);
    res = dst;

    return status;
}


int Algorithm::Process(Mat *Im1, Mat *Im2, Mat *res, Mat *map, int nb, int depth){
    Mat tmp1, tmp2, M;
    Mat cpIm1, cpIm2;
    Mat rgbIm1[3], rgbIm2[3];
    Mat tmpRes[3];
    int status = 0;
    int bl_radius = 5;

    Im1->copyTo(cpIm1);
    Im2->copyTo(cpIm2);

    split(*Im1,rgbIm1);
    split(*Im2, rgbIm2);


    cvtColor(cpIm1,cpIm1,COLOR_RGB2GRAY);
    cvtColor(cpIm2,cpIm2,COLOR_RGB2GRAY);

    GaussianBlur( cpIm1, cpIm1, Size( bl_radius, bl_radius ), 0, 0 );
    GaussianBlur( cpIm2, cpIm2, Size( bl_radius, bl_radius), 0, 0 );

    cpIm1.convertTo(cpIm1, CV_32FC1);
    cpIm2.convertTo(cpIm2, CV_32FC1);

    for(int i = 0; i < 3; i++){
        rgbIm1[i].convertTo(rgbIm1[i], CV_32FC1);
        rgbIm2[i].convertTo(rgbIm2[i], CV_32FC1);
    }

    // Convolution with gradient kernel
    filter2D(cpIm1, tmp1, -1, gradient_window);
    filter2D(cpIm2, tmp2, -1, gradient_window);

    tmp1 = cv::abs(tmp1);
    tmp2= cv::abs(tmp2);

    M =  tmp1 - tmp2;

    //Sum over the regions to increase robustness
    // Convolution with ones matrix
    filter2D(M, M, -1, sum_window);
    CalculateSigmoid(M, M);

    // Join images
    for(int i = 0; i < 3; i++){
        tmp2 = 1-M;
        multiply(M,rgbIm1[i],tmp1);
        multiply(tmp2,rgbIm2[i],tmp2);
        tmpRes[i] = tmp1 + tmp2;
        tmpRes[i].convertTo(tmpRes[i], CV_8UC1);
    }
    merge(tmpRes,3,*res);

    // Generate Map
    multiply(M,*map,tmp1);
    *map = tmp1 + (1-M)*(255/depth)*nb;

    return status;
}

void Algorithm::Invert_Map(Mat *map){
    Mat tmp = Mat(map->size(), CV_32FC1, double(255));
    *map = tmp - (*map);
}
}//namespace alg


#include "controler.h"

namespace ctrl {
int Controler::ReadVectorOfImages(string path_, int nb_){
    int status;
    if(nb_ <= 0){
        cerr<<"[ERROR][ReadVectorOfImages] Wrong number of images to read"<<endl;
        return -1;
    }
    ostringstream tmp;
    std::string  file;
    for(int i = 0; i < nb_; i++){
        tmp << i;
        file = path_ + tmp.str() + ".png";
        Image *im = new Image();
        status = im->ReadImage(file);
        if(status < 0){
            vecColorIm.clear();
            vecGrayIm.clear();
            return -1;
        }
        vecColorIm.push_back((*im));
        im->ConvertToGray();
        vecGrayIm.push_back((*im));
        tmp.str("");
        tmp.clear();
    }
    return 0;
}

void Controler::ShowVectorOfImages(){
    if(vecColorIm.size() <= 0) return;
    namedWindow("window");
    for(int i = 0; i < vecColorIm.size(); i++){
        imshow("window",*vecColorIm[i].ReturnImage());
        waitKey(150);
    }
}
int Controler::JoinImages(){
    if(vecGrayIm.size() < 0){
        cerr<<"No images to join"<<endl;
        return -1;
    }

    Mat res;
    Mat map = Mat((*vecGrayIm[0].ReturnImage()).size(), CV_32FC1, double(0));

    for(int i = 0; i < vecGrayIm.size(); i++){
        if(i == 0)
            Alg1.Process(vecColorIm[i].ReturnImage(),vecColorIm[i+1].ReturnImage(), &res, &map, i+1, vecColorIm.size()+1);
        else{
            Alg1.Process(&res, vecColorIm[i].ReturnImage(), &res, &map, i+1, vecColorIm.size()+1);
        }
    }
    Alg1.Invert_Map(&map);
    imwrite("Res.png", res);
    imwrite("Mapa.png",map);
}

}

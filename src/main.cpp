#include <iostream>
#include <cstdlib>
#include "controler.h"

using namespace std;

int main(int argc, char *argv[])
{
    ctrl::Controler ImJoin;
    string path;
    string dec;
    int nb;
    cout<<"README\nImage files should be named by the following pattern YOUR_NAME_0.png, YOUR_NAME_1.png ..."<<endl;
    cout<<"Use default?(y/n): ";
    cin>>dec;
    if(dec.c_str()[0] == 'y'){
        ImJoin.ReadVectorOfImages("../bug/b", 13);
    }
    else{
        cout<<"Path to images with name without file type(for example: ../bug/YOUR_NAME_): "<<endl;
        cin>>path;
        cout<<"How many images? ";
        cin>>nb;
        if(nb <= 1){
            cerr<<"[ERROR] Wrong image number"<<endl;
            return 0;
        }
        ImJoin.ReadVectorOfImages(path.c_str(), nb);
    }
    ImJoin.JoinImages();
    cout<<"Program end..."<<endl;
    cout<<"Check build folder for results :) "<<endl;
    return 0;
}
